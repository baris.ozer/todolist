import { shallowMount, createLocalVue } from '@vue/test-utils'
import List from "@/components/Tasks/List";
import list from "../mock/list";
import Vue from 'vue';
import Vuetify from "vuetify";

Vue.use(Vuetify)

describe('Home.vue', () => {
    const localVue = createLocalVue()
    let wrapper
    let vuetify

    beforeEach(() => {
        vuetify = new Vuetify()
    })

    it('renders a vue instance', () => {
        wrapper = shallowMount(List, {
            localVue,
            vuetify,
            propsData: {
                todos: list.data
            }
        })
        expect(wrapper.isVueInstance()).toBe(true);
    });

    it('renders the task name',  () => {
        wrapper = shallowMount(List, {
            localVue,
            vuetify,
            propsData: {
                todos: list.data
            }
        })
        const tr = wrapper.findAll('tbody > tr > td').at(1)
        expect(tr.text()).toEqual('Test 1')
    })

    it('renders the error field',  () => {
        wrapper = shallowMount(List, {
            localVue,
            vuetify,
            propsData: {
                todos: []
            }
        })
        const error = wrapper.find('#error')
        expect(error.text()).toEqual('Gösterilecek Görev Bulunmamaktadır')
    })
})
