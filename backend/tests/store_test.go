package tests

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	handler2 "gitlab.com/baris.ozer/todolist/backend/handler"
	todolist "gitlab.com/baris.ozer/todolist/backend/platform"
	"gitlab.com/baris.ozer/todolist/backend/platform/mock_http"
	"net/http"
	"testing"
)

func TestStore(t *testing.T) {
	db, err := sql.Open("sqlite3", "./dbs/store.db")
	db.Exec("DELETE from todolist WHERE ID = 1")

	if err != nil {
		t.Errorf("Database connection error has occurred, err: %v", err)
	}

	todo := todolist.NewTodo(db)

	headers := http.Header{}
	headers.Add("content-type", "application/json")

	w := &mock_http.ResponseWriter{}
	r := &http.Request{
		Header: headers,
	}

	r.Body = mock_http.RequestBody(todolist.PostRequest{
		Content:       "Post Test",
		EstimatedTime: 14,
	})

	handler := handler2.Store(todo)
	handler(w, r)

	result := w.GetBodyString()

	if result != "Success" {
		t.Errorf("Handler did not complete")
	}

	if len(todo.Get()) < 1 {
		t.Errorf("Item did not add")
	}

	if todo.Get()[0].Content != "Post Test" {
		t.Errorf("Item bad")
	}
}
