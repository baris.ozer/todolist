package tests

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	handler2 "gitlab.com/baris.ozer/todolist/backend/handler"
	todolist "gitlab.com/baris.ozer/todolist/backend/platform"
	"gitlab.com/baris.ozer/todolist/backend/platform/mock_http"
	"net/http"
	"net/url"
	"testing"
)

func TestDelete(t *testing.T) {
	db, err := sql.Open("sqlite3", "./dbs/delete.db")
	db.Exec("DELETE from todolist WHERE ID = 1")

	if err != nil {
		t.Errorf("Database connection error has occurred, err: %v", err)
	}

	todo := todolist.NewTodo(db)

	todo.Add(todolist.Item{
		Content:       "Delete Test",
		EstimatedTime: 11,
	})

	rows, _ := todo.DB.Prepare("SELECT * FROM todolist WHERE content=\"Delete Test\";")
	rows.Exec()

	handler := handler2.Delete(todo)

	w := &mock_http.ResponseWriter{}
	r, _ := http.NewRequest("GET", "/", nil)

	q := url.Values{}
	q.Add("id", "1")

	r.URL.RawQuery = q.Encode()

	handler(w, r)

	result := w.GetBodyString()

	if result != "Success" {
		t.Errorf("Handler did not complete")
	}

}
