import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import axios from 'axios'
import VueAxios from 'vue-axios'
import store from './store'
import Toasted from 'vue-toasted'

Vue.use(Toasted, {duration: 750, position: 'top-center'});
Vue.use(VueAxios, axios)

axios.defaults.baseURL = window.location.protocol + '//' + window.location.hostname + ':' + 3000

Vue.config.productionTip = false

new Vue({
    store,
    vuetify,
    render: h => h(App)
}).$mount('#app')
