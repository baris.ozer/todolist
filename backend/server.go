package main

import (
	"database/sql"
	"github.com/go-chi/chi"
	"github.com/go-chi/cors"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/baris.ozer/todolist/backend/handler"
	"gitlab.com/baris.ozer/todolist/backend/platform"
	"log"
	"net/http"
)

func main() {
	newServer()
}

func dbDriver() (*todolist.Todo, error) {
	db, err := sql.Open("sqlite3", "./todolist.db")
	return todolist.NewTodo(db), err
}

// Routers were created using chi. In addition, a db instance is created.
func newServer() {
	todo, _ := dbDriver()
	r := chi.NewRouter()
	r.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: false,
		MaxAge:           300,
	}))

	r.Get("/todo", handler.Index(todo))
	r.Post("/todo", handler.Store(todo))
	r.Delete("/todo", handler.Delete(todo))
	r.Put("/todo", handler.Update(todo))

	http.ListenAndServe(":3000", r)

	log.Println("Server Started...")
}
