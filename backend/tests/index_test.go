package tests

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	handler2 "gitlab.com/baris.ozer/todolist/backend/handler"
	todolist "gitlab.com/baris.ozer/todolist/backend/platform"
	"gitlab.com/baris.ozer/todolist/backend/platform/mock_http"
	"net/http"
	"testing"
)

func TestIndex(t *testing.T) {
	db, err := sql.Open("sqlite3", "./dbs/index.db")
	db.Exec("DELETE from todolist WHERE ID = 1")

	if err != nil {
		t.Errorf("Database connection error has occurred, err: %v", err)
	}

	todo := todolist.NewTodo(db)

	todo.Add(todolist.Item{
		Content:       "Test",
		EstimatedTime: 11,
	})

	handler := handler2.Index(todo)

	w := &mock_http.ResponseWriter{}
	r := &http.Request{}

	handler(w, r)

	result := w.GetBodyJSONArray()

	if len(result) <= 1 {
		t.Errorf("Item was not added to the datastore")
	}

	if result[0]["Content"] != "Test" {
		t.Errorf("Content was not properly set")
	}

	if result[0]["EstimatedTime"] != float64(11) {
		t.Errorf("EstimatedTime was not properly set")
	}
}
