package handler

import (
	"encoding/json"
	"fmt"
	"gitlab.com/baris.ozer/todolist/backend/platform"
	"net/http"
)

func Store(todo todolist.IStore) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// define a var
		var input todolist.PostRequest

		// decode input or return error
		err := json.NewDecoder(r.Body).Decode(&input)
		if err != nil {
			w.WriteHeader(400)
			fmt.Fprintf(w, "Decode error! please check your JSON formating.")
			return
		}

		todo.Add(todolist.Item{
			Content:       input.Content,
			EstimatedTime: input.EstimatedTime,
		})

		w.Write([]byte("Success"))
	}
}
