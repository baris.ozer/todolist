# SIMPLE TODO LIST

**Vue.js ve golang teknolojileri kullanılarak oluşturulmuş basit bir yapılacaklar listesi uygulaması. **

## Kurulum Yerel

### Gereksinimler

* [Docker] 
* [Docker-compose]
* [Golang]
* [Node]

```sh
$ git clone https://gitlab.com/baris.ozer/todolist.git
```

##### Backend
```sh
$ cd backend
$ god mod download
$ go run server.go
```
##### Frontend
```sh
$ cd frontend
$ npm install
$ npm install -g @vue/cli - Yüklü ise geçiniz
$ npm run serve
```

##### Docker İle Kurulum
- Depoyu bilgisayarınıza klonladıktan sonra proje klasörü içerisinde alttaki komutu çalıştırmalısınız.

```sh
$ docker swarm init
$ docker stack deploy -c docker-compose.yml todolist
```

## Kurulum Production

### Sunucu

#### Gereksinimler
* [Docker] 
* [Docker-compose]
* [Gitlab-runner]
* [git]

1. Kurumlumdan önce Gitlab-Runner(Repo aktif olmalı) ve docker kurulumunu tamamlamalısınız.
- Gitlab runner kurulum talimatları = [](https://docs.gitlab.com/runner/install/)
- Docker kurulum talimatları = [](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04)

2. Yeni kullanıcı ve yeni bir dizin eklemelisiniz(Zorunlu değil).
 ```sh
 $ sudo docker swarm init
 $ sudo adduser kullanici
 $ sudo usermod -aG docker kullanici
 $ mkdir yeni/dizin
 $ sudo chown -R kullanici:kullanici /yeni/dizin
 ```
3. Oluşturduğunuz kullanıcı için ssh ayarlarını sunucunuz üzerinde tamamlamalısınız.
4. Gitlab runner için gerekli değişkenler(Gitlab cd/ci ayarlarında bulabilirsiniz):
 * [ID_RSA => Private Key(Tipi dosya olarak seçiniz ve satır sonu eklediğinizden emin olunuz)] 
 * [SERVER_IP => Sunucu Ip]
 * [SERVER_USER => Yeni Kullanıcınız]
5. Daha sonra docker-compose dosyası içerisindeki registry adreslerini kendi adreslerinizle güncelleyin. Gitlab cd/ci değişkeni olarakta eklenebilir. 
Örneğin: registry.gitlab.com/<kullaniciadi>/<repo>/<container>
6. Master dalına her merge alındığında pipeline otomatik olarak çalışacaktır.

#### Veritabanı
> Veritabanı olarak sqlite 3 kullanılmıştır. Scale etmek için uygun bir çözüm değil. Konteynerlar ile ortak bir yolu paylaşabilir ama burada dikkate almadım. Veritabanı driver'ı değiştirilebilir.


#### Test
```sh
$ cd backend && go test -v -cover ./...
$ cd frontend && npm run test:unit
```

