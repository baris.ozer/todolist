package todolist

type PostRequest struct {
	Content       string `json:"content"`
	EstimatedTime int    `json:"EstimatedTime"`
}

type DeleteRequest struct {
	ID int
}

type UpdateRequest struct {
	ID       int `json:"id"`
	IsFinish int `json:"isFinish"`
}
