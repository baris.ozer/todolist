package handler

import (
	"gitlab.com/baris.ozer/todolist/backend/platform"
	"net/http"
	"strconv"
)

func Delete(todo todolist.IDelete) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var id int
		id, _ = strconv.Atoi(r.URL.Query().Get("id"))
		todo.Delete(id)

		w.Write([]byte("Success"))
	}
}
