package handler

import (
	"encoding/json"
	"gitlab.com/baris.ozer/todolist/backend/platform"
	"net/http"
)

func Index(todo todolist.IGet) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		items := todo.Get()
		w.WriteHeader(http.StatusCreated)
		json.NewEncoder(w).Encode(items)
	}
}
