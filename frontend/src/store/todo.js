import axios from 'axios'

const resource = '/todo'
import Vue from 'vue'

export default {
    namespaced: true,

    state: {
        todos: {}
    },

    getters: {
        todos: state => state.todos,
    },

    mutations: {
        setTodos(state, todos) {
            state.todos = todos
        },
        deleteTodo(state, id) {
            let todos = state.todos.filter(x => x.ID !== id);
            state.todos = todos
        },
        updateTodo(state, newTodo) {
            state.todos.forEach(function (item) {
                if (item.ID === newTodo.id) {
                    item.IsFinish = newTodo.value
                }
            });
        }
    },

    actions: {
        async createTodo({dispatch}, {payload}) {
            return axios.post(`${resource}`, payload).then(
                () => {
                    dispatch('getTodos')
                    Vue.toasted.success('İşlem Başarılı')
                }
            ).catch(
                error => {
                    Vue.toasted.error(error)
                }
            )
        },
        async updateTodo({commit}, {id, payload}) {
            return axios.put(`${resource}?id=${id}`, payload).then(
                () => {
                    commit('updateTodo', {id, value: payload.isFinish})
                    Vue.toasted.success('İşlem Başarılı')
                }
            ).catch(
                error => {
                    Vue.toasted.success(error)
                }
            )
        },
        async getTodos({commit}) {
            return await axios.get(`${resource}`).then(
                response => {
                    commit('setTodos', response.data);
                }
            ).catch(
                function (error) {
                    Vue.toasted.error(error)
                }
            )
        },
        async deleteTodo({commit}, id) {
            return axios.delete(`${resource}?id=${id}`).then(
                () => {
                    commit('deleteTodo', id)
                    Vue.toasted.success('İşlem Başarılı')
                }
            ).catch(
                function (error) {
                    Vue.toasted.error(error)
                }
            )
        }
    },
};
