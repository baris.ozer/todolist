package todolist

import (
	"database/sql"
	"log"
)

type Todo struct {
	DB *sql.DB
}

type IGet interface {
	Get() []Item
}

type IDelete interface {
	Delete(id int)
}

type IUpdate interface {
	Update(item UpdateRequest)
}

type IStore interface {
	Add(item Item)
}

func (todo *Todo) Add(item Item) {
	stmt, err := todo.DB.Prepare(`INSERT INTO todolist (content, estimated_time, is_finish) values (?, ?, ?)`)

	if err != nil {
		log.Println(err)
	}

	stmt.Exec(item.Content, item.EstimatedTime, item.IsFinish)
}

func (todo *Todo) Delete(id int) {
	stmt, err := todo.DB.Prepare(`DELETE FROM todolist WHERE ID = (?);`)

	if err != nil {
		log.Println(err)
	}

	stmt.Exec(id)
}

func (todo *Todo) Update(request UpdateRequest) {
	stmt, err := todo.DB.Prepare(`UPDATE todolist SET is_finish = (?) WHERE ID = (?);`)

	if err != nil {
		log.Println(err)
	}
	log.Println(request.IsFinish)
	stmt.Exec(request.IsFinish, request.ID)
}

func (todo *Todo) Get() []Item {
	items := []Item{}

	rows, _ := todo.DB.Query(`SELECT * from todolist ORDER BY ID DESC`)

	var id int
	var content string
	var estimatedTime int
	var isFinish int

	for rows.Next() {
		rows.Scan(&id, &content, &estimatedTime, &isFinish)
		item := Item{
			ID:            id,
			Content:       content,
			EstimatedTime: estimatedTime,
			IsFinish:      isFinish,
		}
		items = append(items, item)
	}
	return items
}

func NewTodo(db *sql.DB) *Todo {
	stmt, _ := db.Prepare(`
		CREATE TABLE IF NOT EXISTS "todolist" (
		"ID" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
		"content" TEXT,
		"estimated_time" INTEGER,
		"is_finish" INTEGER DEFAULT 0);`)

	stmt.Exec()

	return &Todo{
		DB: db,
	}
}
