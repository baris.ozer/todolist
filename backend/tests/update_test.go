package tests

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	handler2 "gitlab.com/baris.ozer/todolist/backend/handler"
	todolist "gitlab.com/baris.ozer/todolist/backend/platform"
	"gitlab.com/baris.ozer/todolist/backend/platform/mock_http"
	"net/http"
	"net/url"
	"testing"
)

func TestUpdate(t *testing.T) {
	db, err := sql.Open("sqlite3", "./dbs/update.db")
	db.Exec("DELETE from todolist WHERE ID = 1")

	if err != nil {
		t.Errorf("Database connection error has occurred, err: %v", err)
	}

	todo := todolist.NewTodo(db)

	headers := http.Header{}
	headers.Add("content-type", "application/json")

	w := &mock_http.ResponseWriter{}
	r, _ := http.NewRequest("GET", "/", nil)

	q := url.Values{}
	q.Add("id", "1")

	r.URL.RawQuery = q.Encode()

	r.Body = mock_http.RequestBody(todolist.UpdateRequest{
		ID:       1,
		IsFinish: 1,
	})

	handler := handler2.Update(todo)
	handler(w, r)

	result := w.GetBodyString()

	if result != "Success" {
		t.Errorf("Handler did not complete")
	}

	for _, v := range todo.Get() {
		if v.IsFinish != 1 {
			t.Errorf("Update not working")
		}
	}

}
