package handler

import (
	"encoding/json"
	"fmt"
	"gitlab.com/baris.ozer/todolist/backend/platform"
	"net/http"
	"strconv"
)

func Update(todo todolist.IUpdate) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		// define a var
		var input todolist.UpdateRequest

		// decode input or return error
		err := json.NewDecoder(r.Body).Decode(&input)

		// set id
		input.ID, _ = strconv.Atoi(r.URL.Query().Get("id"))

		if err != nil {
			w.WriteHeader(400)
			fmt.Fprintf(w, "Decode error! please check your JSON formating.")
			return
		}

		todo.Update(input)

		w.Write([]byte("Success"))
	}
}
