package todolist

type Item struct {
	ID            int
	Content       string
	EstimatedTime int
	IsFinish      int
}
