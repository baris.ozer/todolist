module gitlab.com/baris.ozer/todolist/backend

go 1.15

require (
	github.com/go-chi/chi v1.5.1
	github.com/go-chi/cors v1.1.1
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.6
)
